#!/bin/sh

echo "updating existing packages"
sudo apt-get -y update 

echo "installing java"
sudo apt-get install openjdk-8-jdk
echo "java 8 installed and configured"


echo "installing jenkins"
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get -y install jenkins
echo "jenkins installed now starting"
sudo service jenkins start
echo "jenkins started"

echo "allocated 1 GB swap space"
sudo dd if=/dev/zero of=/swapfile bs=128M count=8
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
echo '/swapfile swap swap defaults 0 0' | sudo tee -a /etc/fstab
echo "swap allocated"

echo "installing docker"
echo "installing docker repository"
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic test"
echo "installing docker engine"
sudo apt -y install docker-ce
sudo usermod -a -G docker azureuser
sudo usermod -a -G docker jenkins
sudo chmod 666 /var/run/docker.sock

